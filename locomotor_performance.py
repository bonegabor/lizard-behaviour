#!./venv/bin/python

import sys
import pandas as pd
from os import path, walk
from utils import locomotor_performance, lp_n_day

def main():
    results = []
    arglen = len(sys.argv)
    assert(
        arglen >= 1
        and arglen <= 3
    ), "Usage: python locomotor_performance.py [-n_day] [filename]"
    if arglen == 3 and sys.argv[1] == '-n_day':
        df = lp_n_day(sys.argv[2])
        df.to_csv('results/locomotor_time_between.csv', index=False)
    elif arglen == 2:
        results.append(locomotor_performance(sys.argv[1]))
        print(results)
    elif arglen == 1:
        dir = 'locomotor-performance'
        for (dirpath, dirnames, filenames) in walk('./data/' + dir + '/'):
            for file in filenames:
                if file.endswith(".csv"):
                    filename = path.join(dirpath, file)
                    print(filename)
                    results.append(locomotor_performance(filename))
        df = pd.DataFrame(results)
        df.to_csv('results/' + dir + '.csv', index=False)


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        raise e