mwr_options = {
    'video_length': 1500.0,
    'time_modifier': lambda time, start: (time - start) * 2 , # nem 1/t kell kivonni, hanem a start idopontját
    'start_key': 'q'
}