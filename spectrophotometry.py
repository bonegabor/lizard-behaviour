#!./venv/bin/python

import pandas as pd
from os import path, walk
from utils import spectrophotometry


def main():
    #spectrophotometry('./data/spectrophotometry/22/02hasoldal/Transmission_09-45-24-557.txt')
    #return
    
    # initialization
    measurements_mode = 'mean' #separate|mean
    wl_mode = 'first' # first|mean
    max_no_of_measure = 3
    results = pd.DataFrame(columns=['lizard_no', 'body_part', 'measurement',
                                    'wavelength', 'value'])
    i = 0
    prev_dirpath = ''
    files = []
    
    # reading and processing measurement files
    for (dirpath, dirnames, filenames) in walk('./data/spectrophotometry/'):
        if dirpath != prev_dirpath:
            prev_dirpath = dirpath
            i = 0
        for file in filenames:
            if file.endswith(".txt") and i < max_no_of_measure:
                filename = path.join(dirpath, file)
                files.append(filename)
                # print(filename)
                results = pd.concat([results, spectrophotometry(filename, wl_mode)])
                i = i + 1


    # writing the results
    if (measurements_mode == 'mean'):
        results = results.groupby(['lizard_no', 'body_part', 'wavelength'], as_index=False).mean()
        
    results.to_csv('results/spectrophotometry/' + measurements_mode + '_' + wl_mode + '.csv', index=False)
    print(str.join('\n', files), file=open('results/spectrophotometry/' + measurements_mode + '_' + wl_mode + '_files.txt', 'w'))


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        raise e
