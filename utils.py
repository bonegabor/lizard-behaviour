import cv2
import numpy as np
import pandas as pd
from os.path import basename
from mwrap_reader_settings import mwr_options
from datetime import datetime

def add_grid(frame, n):
    h, w, c = frame.shape
    for i in range(1, n):
        frame = cv2.line(frame, (0, int(i*h/n)), (int(w), int(i*h/n)),
                         (255, 255, 255), 2)
    for i in range(1, n):
        frame = cv2.line(frame, (int(i*w/n), 0), (int(i*w/n), int(h)),
                         (255, 255, 255), 2)

    return frame

def add_circular_mask(frame, radius, thickness, center = False):
    h, w, c = frame.shape
    if center is False:
        center = (int(w/2), int(h/2))
    frame = cv2.circle(frame, center, radius, (255, 255, 255), thickness)

    return frame

def mouse_handler(event, x, y, flags, data):

    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.circle(data['im'], (x, y), 3, (0, 0, 255), 5, 16)
        cv2.imshow("Image", data['im'])
        if len(data['points']) < 4:
            data['points'].append([x, y])

def get_one_image(cap, position):
    # a 900. frame-ből mintázzuk meg az arénát
    cap.set(cv2.CAP_PROP_POS_FRAMES, 900)
    ret, im = cap.read()
    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
    return im.copy()

def get_four_points(im):

    print('Click on the four corners of the arena -- top left first and bottom left last -- and then hit ENTER')

    # Set up data to send to mouse handler
    data = {}
    data['im'] = im
    data['points'] = []

    # Set the callback function for any mouse event
    cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
    cv2.imshow("Image", data['im'])
    cv2.setMouseCallback("Image", mouse_handler, data)
    
    while len(data['points']) < 4:
        cv2.waitKey(100)

    cv2.destroyAllWindows()

    # Convert array to np.array
    points = np.vstack(data['points']).astype(float)

    return points

def exploration2(filename):
    results = {}

    df = read_mwrap(filename)

    # extracting general information from the filename
    filename_info = basename(filename).split('.')[0].split('_')

    # preparing the results
    results['lizard_id'] = filename_info[0]
    results['activity'] = filename_info[1]
    results['day'] = filename_info[2]
    results['unity_number'] = df[df['key'] == 'w'].shape[0]
    results['retreat'] = sum(df['duration'])
    return results


def exploration(filename):
    results = {}
    pattern = [7, 8, 9, 6, 3]

    # reading the .mw file
    df = read_mwrap(filename)
    
    # dropping unnecessary rows
    df = df.loc[df['key'].str.isnumeric() | df['key'].isin(['q', 'Q', 'z'])]

    # calculating the time spent in the different regions
    unity_times(df)

    # calculating times spent in the retreat
    retreat_times(df)

    # extracting general information from the filename
    filename_info = basename(filename).split('.')[0].split('_')

    # preparing the results
    results['lizard_id'] = filename_info[0]
    results['activity'] = filename_info[1]
    results['day'] = filename_info[2]
    results['unity_number'] = df[df['key'].str.isnumeric()].shape[0]

    for i in range(1, 10):
        results[str(i) + '_number'] = df[df['key'] == str(i)].shape[0]

    for i in range(1, 10):
        colname = str(i) + '_time'
        results[colname] = df[df['key'] == str(i)].sum()['unity_time']

    exclude_retreats = df.loc[:, 'key'].replace(['q', 'Q', 'z'], '-1')

    r_sum = df[df['key'] == 'q'].sum()
    results['retreat_1'] = r_sum['r1']
    results['retreat_2'] = r_sum['r2']

    results['cw'] = find_pattern(exclude_retreats, pattern)
    results['ccw'] = find_pattern(exclude_retreats, pattern[::-1])
    return results


def risk_taking(filename):
    results = {}

    df = read_mwrap(filename)

    if not isinstance(df, pd.DataFrame):
        return results

    filename_info = basename(filename).split('.')[0].split('_')

    results['lizard_id'] = filename_info[0]
    results['activity'] = filename_info[1]
    results['day'] = filename_info[2]

    retreat_times(df)
    r_sum = df[df['key'] == 'q'].sum()
    results['retreat_1'] = r_sum['r1']
    results['retreat_2'] = r_sum['r2']

    basking_times(df)
    tb_sum = df[df['key'] == 'z'].sum()
    results['till_basking'] = tb_sum['bt1']
    b_sum = df[df['key'] == 'a'].sum()
    results['bask'] = b_sum['bt1']

    return results

def risk_taking2(filename):
    results = {}

    df = read_mwrap(filename)
    
    if not isinstance(df, pd.DataFrame):
        return results

    filename_info = basename(filename).split('.')[0].split('_')

    results['lizard_id'] = filename_info[0]
    results['activity'] = filename_info[1]
    results['day'] = filename_info[2]

    #retreat_times(df)
    results['retreat'] = df[df['key'] == 'Q']['duration'].sum()
    try:
        results['till_basking'] = df[df['key'] == 'a']['time'].iloc[0]
    except:
        results['till_basking'] = 9999
        
    results['bask'] = df[df['key'] == 'A']['duration'].sum()

    return results

def spectrophotometry(filename, method):
    import re

    content = []
    with open(filename) as f:
        spectral_data = False
        for line in f:
            line = line.strip()
            if spectral_data is False:
                if line == '>>>>>Begin Spectral Data<<<<<':
                    spectral_data = True
                else:
                    continue
            else:
                content.append(line.split('\t'))

    df = pd.DataFrame(content, columns=['wavelength_prec', 'value'])

    df['lizard_no'] = filename[25:27]
    df['body_part'] = re.search(r"(?<=\d)[a-z]{5,8}(?=/)", filename).group(0)
    df['measurement'] = re.search(r"(?<=Transmission_)[0-9\-]*(?=.txt)",
                                  basename(filename)).group(0)
    df['wavelength_prec'] = df['wavelength_prec'].astype(float)
    df['wavelength'] = df['wavelength_prec'].astype(int)
    df['value'] = df['value'].astype(float)

    # filtering values
    df = df[df['wavelength'] % 5 == 0]
    df = df[df['wavelength_prec'] >= 300]
    df = df[df['wavelength_prec'] <= 700]

    if (method == 'first'):
        wl_mins = df.groupby(['wavelength']).min()['wavelength_prec']

        resutls = df.loc[(df['wavelength_prec'].isin(wl_mins)),
                        ['lizard_no', 'body_part', 'measurement', 'wavelength',
                         'value']]
    elif (method == 'mean'):
        resutls = df.groupby(['lizard_no', 'body_part', 'measurement', 'wavelength'], as_index=False).mean()
        resutls = resutls.loc[:,('lizard_no', 'body_part', 'measurement', 'wavelength', 'value')]
    return resutls


def read_mwrap(filename):
    content = []
    columns = ['id', 'description', 'key', 'time', 'duration', 'object id']
    end_line = pd.DataFrame({
        'id': 10000,
        'description': 'finish',
        'key': '99',
        'time': mwr_options['video_length'],
        'duration': 0.00,
        'object id': 1
    }, index=[0])

    with open(filename) as f:
        for line in f:
            line = line.strip()
            if line[0] != '#':
                content.append(line.split(';'))

    df = pd.DataFrame(content)

    if df.empty:
        return False

    df.columns = columns

    # Convert values to numeric
    for num in ['id', 'time', 'duration', 'object id']:
        df[num] = pd.to_numeric(df[num])

    # sort activity by time
    df.sort_values(by=['time'], inplace=True)
    start_time = df[df['key'] == mwr_options['start_key']]['time'].iloc[0]
    
    # apply the tim emodifier lamba function
    df['time'] = mwr_options['time_modifier'](df['time'], start_time)
    df['duration'] = mwr_options['time_modifier'](df['duration'], 0)

    # drop events after one hour
    df = df[df['time'] < mwr_options['video_length']]

    # drop invalid events
    df = df[df['description'] != 'undefined']

    # adding a finish line to the end of the df, for calculating the duration
    # of last event
    df = pd.concat([df, end_line], ignore_index=True)

    return df


def retreat_times(df):
    retreats = df.copy()
    
    retreats = retreats.loc[(retreats['key'].isin(['q', 'Q', 'z', '99'])), ['id', 'key', 'time']]
    
    retreats['num_key'] = retreats['key'].replace(['q', 'Q', 'z', '99'],
                                                  [0, 1, 2, 99])

        # ez
    #    retreats['valid_qq'] = retreats['num_key'].rolling(2) \
    #        .apply(lambda x: all(np.equal(x, [0, 1])))
    #    retreats['valid_qqz'] = retreats['num_key'].rolling(3) \
    #        .apply(lambda x: all(np.equal(x, [0, 1, 2])))
    if retreats.iloc[0]['key'] != 'q':
        # raise ValueError('First q is missing in file:')
        df['r1'] = -1
        df['r2'] = -1
        return
    if retreats.iloc[-2]['key'] == 'q':
        retreats = pd.concat([retreats, pd.DataFrame({
            'id': 9999,
            'key': 'Q',
            'num_key': 1,
            'time': mwr_options['video_length']
            }, index=[9999])]).sort_values(by=['id', 'key'])

    errors = retreats['num_key'].rolling(2) \
        .apply(lambda x: all(np.equal(x, [0, 2]))
               | all(np.equal(x, [2, 1]))
               | all(np.equal(x, [0, 0]))
               | all(np.equal(x, [1, 1]))
               | all(np.equal(x, [2, 2]))
               )
    if errors.sum() > 0:
        # raise ValueError('Errors found in retreat order, please review it')
        df['r1'] = df['r2'] = -1
        return

    missing_z = retreats['num_key'].rolling(3, center=True) \
        .apply(lambda x: all(np.equal(x, [0, 1, 99]))
               | all(np.equal(x, [0, 1, 0])))

    retreats = pd.concat([retreats, retreats[missing_z == 1]
                         .replace('Q', 'z')]).sort_values(by=['id', 'key'])

    retreats['num_key'] = retreats['key'].replace(['q', 'Q', 'z'],
                                                  [0, 1, 2])

    # calculating durations between q and Q, q and z
    retreats.loc[:, 'r1'] = retreats['time'].shift(-1) - retreats['time']
    retreats.loc[:, 'r2'] = retreats['time'].shift(-2) - retreats['time']

    # wrinting results back to df
    df['r1'] = df.id.map(retreats[retreats['key'] == 'q']
                         .set_index('id')['r1'])
    df['r2'] = df.id.map(retreats[retreats['key'] == 'q']
                         .set_index('id')['r2'])


def basking_times(df):
    bk = df.copy()
    bk = bk.loc[(bk['key'].isin(['z', 'a', 'A', '99'])),
                ['id', 'key', 'time']]
    bk['num_key'] = bk['key'].replace(['z', 'a', 'A', '99'],
                                      [0, 1, 2, 99])

    # First z is missing
    if bk.iloc[0]['key'] != 'z':
        df['bt1'] = -1
        df['bt2'] = -1
        return

    # last A is missing
    if bk.iloc[-2]['key'] == 'a':
        bk = pd.concat([bk, pd.DataFrame({
            'id': 9999,
            'key': 'A',
            'num_key': 2,
            'time': 3600
            }, index=[9999])]).sort_values(by=['id', 'key'])

    errors = bk['num_key'].rolling(2) \
        .apply(lambda x: all(np.equal(x, [0, 2]))
               | all(np.equal(x, [0, 2]))
               | all(np.equal(x, [1, 0]))
               | all(np.equal(x, [1, 1]))
               | all(np.equal(x, [2, 2]))
               )

    if errors.sum() > 0:
        # raise ValueError('Errors found in retreat order, please review it')
        df['bt1'] = df['bt2'] = -1
        return

    # calculating durations between q and Q, q and z
    bk.loc[:, 'bt1'] = bk['time'].shift(-1) - bk['time']

    # wrinting results back to df
    df['bt1'] = df.id.map(bk[bk['key'].isin(['z', 'a'])]
                          .set_index('id')['bt1'])


def unity_times(df):
    unities = df.copy()
    unities = unities[
        (unities['key'].str.isnumeric()) | (unities['key'] == 'q')
    ]
    unities.loc[:, 'unity_time'] = unities['time'].shift(-1) - unities['time']
    df['unity_time'] = df.id.map(unities.set_index('id')['unity_time'])


def find_pattern(df, pattern):
    matched = df.rolling(len(pattern)) \
        .apply(lambda x: all(np.equal(x, pattern)))

    return matched[matched == 1].count()

def calculate_inscribed_circle(rectangle_corners):
    # Assuming rectangle_corners is a list of tuples representing the coordinates of the rectangle corners
    # For example, rectangle_corners = [(x1, y1), (x2, y2), (x3, y3), (x4, y4)]

    # Calculate the midpoints of the diagonals
    diagonal_midpoint1 = ((rectangle_corners[0][0] + rectangle_corners[2][0]) / 2, (rectangle_corners[0][1] + rectangle_corners[2][1]) / 2)
    diagonal_midpoint2 = ((rectangle_corners[1][0] + rectangle_corners[3][0]) / 2, (rectangle_corners[1][1] + rectangle_corners[3][1]) / 2)

    # Calculate the center of the inscribed circle as the midpoint of the diagonals
    center_x = int((diagonal_midpoint1[0] + diagonal_midpoint2[0]) / 2)
    center_y = int((diagonal_midpoint1[1] + diagonal_midpoint2[1]) / 2)

    # Calculate the radius as half the length of one side of the square
    side_length = max(rectangle_corners[2][0] - rectangle_corners[0][0], rectangle_corners[1][1] - rectangle_corners[0][1])
    radius = int(side_length / 2)

    return (center_x, center_y), radius

def locomotor_performance(filename):
    df = pd.read_csv(filename)
    results = {}

    # extracting general information from the filename
    filename_info = basename(filename).split('.')[0].split('_')

    # preparing the results
    results['lizard_id'] = filename_info[0]
    results['activity'] = filename_info[1]
    results['day'] = filename_info[2]
    results['time'] = lp_time(df)
    results['distance'] = lp_total_distance(df)
    results['avg_sprint_speed'] = lp_average_speed(df)
    results['max_speed_25'] = lp_max_speed_on_distance(df, 25)
    results['max_speed_50'] = lp_max_speed_on_distance(df, 50)
    results['max_speed_75'] = lp_max_speed_on_distance(df, 75)
    results['max_speed_100'] = lp_max_speed_on_distance(df, 100)

    return results

def lp_time(df):
    first_valid_index = df['x1'].first_valid_index()
    last_valid_index = df['x1'].last_valid_index()

    # Calculate the number of frames
    number_of_frames = 0
    if first_valid_index is not None and last_valid_index is not None:
        number_of_frames = last_valid_index - first_valid_index + 1
    
    return number_of_frames * 0.04

def lp_total_distance(df):
    # Initialize variables
    total_distance = 0.0
    current_chunk_distance = 0.0
    last_valid_index = None

    # Iterate through the DataFrame
    for i in range(0, len(df)):
        if pd.notna(df.loc[i, 'x1']) and pd.notna(df.loc[i, 'y1']):
            # Valid coordinates, calculate distance
            if last_valid_index is not None:
                x1, y1 = df.loc[last_valid_index, 'x1'], df.loc[last_valid_index, 'y1']
                x2, y2 = df.loc[i, 'x1'], df.loc[i, 'y1']
                current_chunk_distance += calculate_distance(x1, y1, x2, y2)
            last_valid_index = i
        else:
            # Missing coordinates, end current chunk
            total_distance += current_chunk_distance
            current_chunk_distance = 0.0
            last_valid_index = None

    # Add distance of the last chunk
    total_distance += current_chunk_distance
    return total_distance
    #print(f'Total distance traveled by the lizard: {total_distance:.2f} cm')

# Function to calculate average speed
def calculate_average_speed(distance, time):
    if time == 0:
        return 0.0
    return distance / time


def lp_average_speed(df):
    # Initialize variables
    total_distance = 0.0
    total_time = 0.0
    current_chunk_distance = 0.0
    current_chunk_time = 0.0
    last_valid_index = None

    # Iterate through the DataFrame
    for i in range(0, len(df)):
        if pd.notna(df.loc[i, 'x1']) and pd.notna(df.loc[i, 'y1']):
            # Valid coordinates, calculate distance and time
            if last_valid_index is not None:
                x1, y1 = df.loc[last_valid_index, 'x1'], df.loc[last_valid_index, 'y1']
                x2, y2 = df.loc[i, 'x1'], df.loc[i, 'y1']
                current_chunk_distance += calculate_distance(x1, y1, x2, y2)
                current_chunk_time += 1  # Assuming each frame represents 1 unit of time
            last_valid_index = i
        else:
            # Missing coordinates, end current chunk
            total_distance += current_chunk_distance
            total_time += current_chunk_time
            current_chunk_distance = 0.0
            current_chunk_time = 0.0
            last_valid_index = None

    # Add distance and time of the last chunk
    total_distance += current_chunk_distance
    total_time += current_chunk_time

    # Calculate average speed
    average_speed = calculate_average_speed(total_distance, total_time)    
    return average_speed


def lp_max_speed_on_distance(df, max_distance):
    last_valid_index = None
    total_distance = 0.0
    total_time = 0.0
    current_chunk_distance = 0.0
    current_chunk_time = 0.0

    max_speed = 0.0
    max_speed_section_info = {}
    # Iterate through the DataFrame
    for h in range(0, len(df)):
        #print(f'HH: {h}')
        total_distance += current_chunk_distance
        total_time += current_chunk_time
        current_chunk_distance = 0.0
        current_chunk_time = 0.0
        last_valid_index = None

        if pd.notna(df.loc[h, 'x1']) and pd.notna(df.loc[h, 'y1']):
            for i in range(h, len(df)):
                if pd.notna(df.loc[i, 'x1']) and pd.notna(df.loc[i, 'y1']):
                    # Valid coordinates, calculate distance and time
                    if last_valid_index is not None:
                        x1, y1 = df.loc[last_valid_index, 'x1'], df.loc[last_valid_index, 'y1']
                        x2, y2 = df.loc[i, 'x1'], df.loc[i, 'y1']
                        d = calculate_distance(x1, y1, x2, y2)
                        
                        current_chunk_distance += d
                        current_chunk_time += 0.04  # Assuming each frame represents 1 unit of time
                    last_valid_index = i                
                else:
                    break

                if current_chunk_distance > max_distance:
                    current_chunk_speed = calculate_average_speed(current_chunk_distance, current_chunk_time)
                    if max_speed < current_chunk_speed:
                        max_speed = current_chunk_speed
                        max_speed_section_info = {
                            'start': h,
                            'end': i,
                            'd': current_chunk_distance,
                            't': current_chunk_time
                        }
                    #print(f'from {h} to {i}: last_d = {d}; d = {current_chunk_distance}; t = {current_chunk_time}; v = {current_chunk_distance / current_chunk_time}')
                    break

            current_chunk_distance = 0.0
            last_valid_index = None
        else:
            #print('==== END CHUNK ====')
            # Missing coordinates, end current chunk
            total_distance += current_chunk_distance
            total_time += current_chunk_time
            current_chunk_distance = 0.0
            current_chunk_time = 0.0
            last_valid_index = None

    #print(f'MAX SPEED: {max_speed}')
    #print(max_speed_section_info)   
    return max_speed

def lp_n_day(filename):
    df = pd.read_csv(filename)
    results = []

    lizard_ids = df['ID'].unique()

    for lid in lizard_ids:
        l_df = df[df['ID'] == lid]
        previous_value = False
        for k, row in l_df.iterrows():
            date_string = row['calendar_day'] + ' ' + row['hour']
            format_date = datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S')
            if previous_value == False:
                results.append(0)
            else:
                results.append((format_date - previous_value).seconds / 60)
            previous_value = format_date
                
    
    df['time_between'] = results
    return df

# Function to calculate Euclidean distance
def calculate_distance(x1, y1, x2, y2):
    return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5