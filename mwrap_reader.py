#!./venv/bin/python

import sys
import pandas as pd
from os import path, walk
from utils import exploration, risk_taking, exploration2, risk_taking2


def main():
    results = []
    arglen = len(sys.argv)
    assert(
        arglen >= 2
        and arglen <= 3
        and (sys.argv[1] == 'exploration' or sys.argv[1] == 'exploration2' or sys.argv[1] == 'risk-taking' or sys.argv[1] == 'risk-taking2')
    ), "Usage: python mwrap_reader.py exploration|risk-taking [filename]"
    if arglen > 2:
        if sys.argv[1] == 'exploration':
            results.append(exploration(sys.argv[2]))
        elif sys.argv[1] == 'risk-taking':
            results.append(risk_taking(sys.argv[2]))
        elif sys.argv[1] == 'exploration2':
            results.append(exploration2(sys.argv[2]))
        elif sys.argv[1] == 'risk-taking2':
            results.append(risk_taking2(sys.argv[2]))
        print(results)
    else:
        dir = sys.argv[1]
        for (dirpath, dirnames, filenames) in walk('./data/' + dir + '/'):
            for file in filenames:
                if file.endswith(".mw.csv"):
                    filename = path.join(dirpath, file)
                    print(filename)
                    if sys.argv[1] == 'exploration':
                        results.append(exploration(filename))
                    elif sys.argv[1] == 'risk-taking':
                        results.append(risk_taking(filename))
                    elif sys.argv[1] == 'exploration2':
                        results.append(exploration2(filename))
                    elif sys.argv[1] == 'risk-taking2':
                        results.append(risk_taking2(filename))
        df = pd.DataFrame(results)
        df.to_csv('results/' + dir + '.csv', index=False)


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        raise e
