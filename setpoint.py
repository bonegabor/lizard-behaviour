import csv
import statistics
import re
import os

def process_folder(folder_path, output_csv):
    # List to store results
    all_results = []

    # Iterate through each file in the folder
    for filename in os.listdir(folder_path):
        file_path = os.path.join(folder_path, filename)

        # Check if the file is a text file
        if filename.endswith(".csv"):
            # Process the file and get the result
            result = process_file(file_path)

            # Append the result to the list
            if result:
                all_results.append(result)

    # Write the results to a CSV file
    with open(output_csv, 'w', newline='') as csvfile:
        fieldnames = list(all_results[0].keys())
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        # Write header
        writer.writeheader()

        # Write data
        for result in all_results:
            writer.writerow(result)

def process_file(file_path):
    try:
        # Extracting ID, day, and hour from the filename
        file_name_pattern = r'(\d+)_(\d{4}\.\d{2}\.\d{2})_(\d{2}\.\d{2})\.csv'
        match = re.match(file_name_pattern, os.path.basename(file_path))
    
        if not match:
            print(f"Filename '{file_path}' does not match the expected pattern.")
            return None

        ID, day, hour = match.groups()
        
        # Step 1: Read the text file
        with open(file_path, 'r') as file:
            lines = file.readlines()

        # Step 2: Drop the first two rows
        lines = lines[2:]

        # Step 3: Split the content into 4-row chunks
        chunks = [lines[i:i + 4] for i in range(0, len(lines), 4)]
        
        result_dict = {'ID': ID, 'day': day.replace('.','-'), 'hour': hour.replace('.',':')}

        # Step 4 and 5: Extract code from the first row and values from the third row of each chunk
        for chunk in chunks:
            code_mapping = {'El1': 'Thead', 'El2': 'Tmidbody', 'El3': 'Ttailbase', 'El4': 'Ttail'}

            code = chunk[0].strip().replace(',','')

            values = chunk[2].strip().split(',')
            values = [float(val) for val in values[1:] if val]

            # Calculate mean, standard deviation, and count
            mean_val = statistics.mean(values) if len(values) else False
            sd_val = statistics.stdev(values) if len(values) else False
            count_val = len(values) if len(values) else False
            
            if (code not in code_mapping.keys()):
                continue

            code_name = code_mapping.get(code)
            
            # Create keys based on code and position (1 to 4)
            keys = [f"{code_name}_mean", f"{code_name}_sd", f"{code_name}_count"]

            # Assign calculated values to the dictionary
            for key, value in zip(keys, [mean_val, sd_val, count_val]):
                result_dict[key] = result_dict.get(key, 0) + value
        
        return result_dict
    except Exception as e:
        print(f"Error processing file '{file_path}': {e}")
        return None

folder_path = 'data/setpoint'
output_csv = 'results/setpoint_results.csv'
process_folder(folder_path, output_csv)