#!./venv/bin/python

import sys
import numpy as np
import cv2
import os.path
from utils import get_one_image
from utils import get_four_points
from utils import add_circular_mask
from utils import calculate_inscribed_circle
import argparse

try:
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('filename')
    parser.add_argument('-v', '--show_video', action='store_true')
    parser.add_argument('-m', '--mask', action='store_true')
    args = parser.parse_args()

    # checking if file exists
    assert(os.path.isfile(args.filename)), args.filename + " file not found"

    cap = cv2.VideoCapture(args.filename)
    fps = cap.get(cv2.CAP_PROP_FPS)
    im = get_one_image(cap, 900)
    # Show fram #300 and wait for 4 clicks.
    o_pts_src = get_four_points(im)
    
    o_pts_rot = [
        [o_pts_src[3][0], o_pts_src[0][1]],
        [o_pts_src[1][0], o_pts_src[0][1]],
        [o_pts_src[1][0], o_pts_src[2][1]],
        [o_pts_src[3][0], o_pts_src[2][1]],
    ]
    o_pts_rot = np.vstack(o_pts_rot).astype(float)

    lizard = input('Lizard number: ')

    arena_w = 1000
    arena_l = 1000

    im_dst = np.zeros((arena_w, arena_l, 3), np.uint8)
    pts_dst = np.array([[0, 0], [arena_w - 1, 0], [arena_w - 1, arena_l - 1],
                       [0, arena_l - 1]], dtype=float)

    # Calculate the homography
    hom, status = cv2.findHomography(o_pts_rot, pts_dst)

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    filename = "%s.avi" % (lizard.zfill(2))
    out = cv2.VideoWriter(filename, fourcc, fps, (arena_w, arena_l + 50))

    # label stripe for the bottom of the video
    label = np.zeros((50, arena_w, 3), np.uint8)
    font = cv2.FONT_HERSHEY_SIMPLEX
    labeltext = "lizard: %s" % \
        (lizard)
    label = cv2.putText(label, labeltext, (20, 20), font, 0.6, (255, 255, 255))

    # setup initial location of window
    r, h, c, w = 250, 90, 400, 125  # simply hardcoded the values
    track_window = (c, r, w, h)
    if args.mask:
        # get inner circle bounds
        im = cv2.warpPerspective(get_one_image(cap, 900), hom, (arena_w, arena_l))
        
        i_pts_src = get_four_points(im)
        i_pts_rot = [
            (int(i_pts_src[3][0]), int(i_pts_src[0][1])),
            (int(i_pts_src[1][0]), int(i_pts_src[0][1])),
            (int(i_pts_src[1][0]), int(i_pts_src[2][1])),
            (int(i_pts_src[3][0]), int(i_pts_src[2][1])),
        ]

        center, radius = calculate_inscribed_circle(i_pts_rot)

except AssertionError as error:
    print(error)

else:

    while (cap.isOpened()):
        ret, frame = cap.read()
        if (ret is True):
            # arena1 = frame[arena_y1:arena_y2,arena_x1:arena_x2]
            # arena1 = cv2.resize(arena1,(arena_w,arena_l))
            arena1 = cv2.warpPerspective(frame, hom, (arena_w, arena_l))

            if args.mask:
                arena1 = add_circular_mask(arena1, radius, cv2.FILLED, center)
                arena1 = add_circular_mask(arena1, 750, 500)
            arena1 = np.concatenate((arena1, label), axis=0)
            out.write(arena1)

            if (args.show_video):
                cv2.namedWindow('fg', cv2.WINDOW_NORMAL)
                cv2.imshow('fg', arena1)

                k = cv2.waitKey(1) & 0xFF

                if k == ord('q'):
                    break
        else:
            break
    cap.release()
    out.release()
    cv2.destroyAllWindows()


#i_pts_src = get_four_points(cap)
#    
#    
#    i_pts_rot = np.vstack(i_pts_rot).astype(float)