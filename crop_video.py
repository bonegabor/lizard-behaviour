#!./venv/bin/python

import sys
import numpy as np
import cv2
import os.path
from utils import get_four_points
from utils import add_grid

try:
    if (sys.argv[1] == '-v'):
        video = sys.argv[2]
        show_video = True
    else:
        video = sys.argv[1]
        show_video = False

    # checking if file exists
    assert(os.path.isfile(video)), video + " file not found"

    cap = cv2.VideoCapture(video)

    # Show fram #300 and wait for 4 clicks.
    pts_src = get_four_points(cap)

    lizard = input('Lizard number: ')
    day = input('Day: ')
    behaviour = int(input('Behaviour \n\
        [1] Exploration,\n\
        [2] Risk taking:\n '))

    assert(behaviour in [1, 2]), "possible behaviour values: 1 or 2"

    behaviour = 'exploration' if (behaviour == 1) else 'risk-taking'

    # arena_x1 = int(input('X coordinate of top left corner: '))
    # arena_y1 = int(input('Y coordinate of top left corner: '))

    # arena_x2 = arena_x1 + int(input('Width of arena: '))
    # arena_y2 = arena_y1 + int(input('Length of arena: '))

except IndexError:
    print('Usage: ' + str(sys.argv[0]) + ' path/to/video')

except AssertionError as error:
    print(error)

else:

    arena_w = 510
    arena_l = 900

    im_dst = np.zeros((arena_w, arena_l, 3), np.uint8)
    pts_dst = np.array([[0, 0], [arena_w - 1, 0], [arena_w - 1, arena_l - 1],
                       [0, arena_l - 1]], dtype=float)

    # Calculate the homography
    hom, status = cv2.findHomography(pts_src, pts_dst)

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    filename = "%s_%s_%s.avi" % (lizard.zfill(2), behaviour, day)
    out = cv2.VideoWriter(filename, fourcc, 2.0, (arena_w, arena_l + 50))

    # label stripe for the bottom of the video
    label = np.zeros((50, arena_w, 3), np.uint8)
    font = cv2.FONT_HERSHEY_SIMPLEX
    labeltext = "lizard: %s  |  behaviour: %s  |  day: %s" % \
        (lizard, behaviour, day)
    label = cv2.putText(label, labeltext, (20, 20), font, 0.4, (255, 255, 255))
    labeltext = "TL: (%s, %s) | TR: (%s, %s) | BR: (%s, %s) | BL: (%s, %s)" % \
        (pts_src[0][0], pts_src[0][1], pts_src[1][0], pts_src[1][1],
         pts_src[2][0], pts_src[2][1], pts_src[3][0], pts_src[3][1])
    label = cv2.putText(label, labeltext, (20, 40), font, 0.3, (255, 255, 255))

    # setup initial location of window
    r, h, c, w = 250, 90, 400, 125  # simply hardcoded the values
    track_window = (c, r, w, h)

    while (cap.isOpened()):
        ret, frame = cap.read()
        if (ret is True):
            # arena1 = frame[arena_y1:arena_y2,arena_x1:arena_x2]
            # arena1 = cv2.resize(arena1,(arena_w,arena_l))
            arena1 = cv2.warpPerspective(frame, hom, (arena_w, arena_l))

            arena1 = add_grid(arena1, 3)

            arena1 = np.concatenate((arena1, label), axis=0)
            out.write(arena1)

            if (show_video):
                cv2.namedWindow('fg', cv2.WINDOW_NORMAL)
                cv2.imshow('fg', arena1)

                k = cv2.waitKey(1) & 0xFF

                if k == ord('q'):
                    break
        else:
            break
    cap.release()
    out.release()
    cv2.destroyAllWindows()
